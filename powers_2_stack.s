.data
# gcc -no-pie -o hello  hello.s -nostdlib
num: .asciz "%d\n"
num_in: .asciz "%ld"


.text

.globl main
main:
    push    %rbp
    mov     %rsp, %rbp

    # allocate local space for inputs
    sub     $16, %rsp 
    
    # put inputs into allocated space
    call _user_num
    movq     %rax, (%rsp)
    call _user_num
    movq     %rax, 8(%rsp)

    # call power subroutine
    call _power

    # clear stack
    subq    $16, %rsp

    # print values
    movq    %rax, %rsi
    movq    $0, %rax
    movq    $num, %rdi
    call    printf

    # exit
    call exit
    movq %rbp, %rsp
    pop %rbp

/**
 * Preforms a simple power function
 * cdecl standard attempt;)
 * 
 * (stack params top to bottom)
 * @param 64 bit base value.
 * @param 64 bit exponent value.
 * @return rax^rcx in stack
 */
_power:
    # create stack frame
    push    %rbp
    mov     %rsp, %rbp
    
    # save arguments to registers

    mov     $0, %r10        # interator
    mov     16(%rbp), %r8   # base
    mov     24(%rbp), %rcx  # exponent
    mov     $1, %rax        # rax at n^0
    
    # use RCX for iterator
    loop:
        mulq    %r8             
        
        inc    %r10
        cmpq    %rcx, %r10  # compare iterator and exponent
        jge      end        # end loop if less/equal 
        jmp loop
    end:
        movq %rbp, %rsp
        pop %rbp
        ret

_user_num:
    # preserve old value
    pushq    %rbp
    movq     %rsp, %rbp
    
    subq    $16, %rsp       # Reserve stack space STACK ALLIGNMENT hence 16
    leaq    -16(%rbp) ,%rsi # Allocate
    movq    $num_in , %rdi     # scanf memory arg
    movq    $0 , %rax       # no vector
    call    scanf           # scanf

    popq     %rax            # pop scanf
    addq     $8, %rsp        # 16 bit allignment
    
    movq %rbp, %rsp
    popq %rbp
    ret
