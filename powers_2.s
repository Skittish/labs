.data
# gcc -no-pie -o hello  hello.s -nostdlib
num: .asciz "%d\n"
num_in: .asciz "%ld"


.text

.globl main
main:
    # preserve old value
    push %rbp
    mov %rsp, %rbp

    # get inputs
    call    _user_num       # base
    movq    %rax, %rbx      # temp move to avoid stack dissalignment
    call    _user_num       # exponent

    # swap rax and rbx due to function requironments
    pushq   %rax
    movq    %rbx, %rax
    popq    %rcx

    call    _power
    
    movq    %rax, %rsi
    movq    $0, %rax
    movq    $num, %rdi
    call    printf

    
    call    exit
    
    movq    %rbp, %rsp
    pop     %rbp

/**
 * Preforms a simple power function
 *
 * @param rax 64 bit base value.
 * @param rcx 64 bit exponent value.
 * @return rax^rcx in rax
 */
_power:
    # boilerplate
    push %rbp
    mov %rsp, %rbp
    # save arguments somewhere else to free registers for printing
    movq    $0, %r10    # interator
    movq    %rax, %r8   # base
    movq    $1, %rax    # rax starts at n^0
    # use RCX for iterator
    loop:   
        cmpq    %rcx, %r10       # compare iterator and exponent
        jge      end                # end loop if less/equal 

        mulq    %r8             
        
        inc    %r10
        
        jmp loop
    end:
        movq %rbp, %rsp
        pop %rbp
        ret

/**
 * Gets single 64bit input fro user
 *
 * @param $num_in must be loading string
 * @return user input in rax
 */

_user_num:
    # preserve old value
    pushq    %rbp
    movq     %rsp, %rbp
    
    subq    $16, %rsp       # Reserve stack space STACK ALLIGNMENT hence 16
    leaq    (%rsp) ,%rsi # Allocate
    movq    $num_in , %rdi     # scanf memory arg
    movq    $0 , %rax       # no vector
    call    scanf           # scanf

    popq     %rax            # pop scanf
    addq     $8, %rsp        # 16 bit allignment
    
    movq %rbp, %rsp
    popq %rbp
    ret
