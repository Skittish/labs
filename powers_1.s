.data
# gcc -no-pie -o hello  hello.s -nostdlib
hello: .string "Hello world!\n"
name: .string "Zvonimir Bednarcik!\n"
student_n: .string  "570560\n"

.text
.globl main
main:
    movq    $0, %rax
    movq    $hello, %rdi
    call    printf

    movq    $0, %rax
    movq    $name, %rdi
    call    printf

    movq    $0, %rax
    movq    $student_n, %rdi
    call    printf